#include "widget.h"
#include "ui_widget.h"

#include <QIcon>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)

{
    ui->setupUi(this);

    this->setWindowTitle("Game Project Slimey");
    this->setWindowIcon( QIcon(":/images/Images/slimey.png") );

    //Changing width and height of the window should be done in UI,
    //Code below will help automatically adjust other objects
    qreal margins = 24; //**not automatic, margins of sides + layoutSpacing in UI
    qreal scene_w = width() - margins;
    qreal scene_h = height() - margins;

    scene = new Scene(this, scene_w, scene_h);

    ui->graphicsView->setScene(scene);
}


Widget::~Widget()
{
    delete ui;
}

