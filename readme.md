# This Project

This is my project for PIC10C Spring 2020. Code created and run using Qt Creator. [Quick-link to my learning experience.](#markdown-header-the-development)

## The General Idea

A cross between the Google Dinousar game and Flappy Bird - in that I used a Flappy Bird tutorial to create a jumping game. The player controls a sprite and must avoid obstacles headed its way by jumping (pressing the spacebar).

## Credit:

I followed the tutorial for help with my code (mostly the PillarItem Class and animations) :

- Flappy Bird Game in Qt by LearnQtGuide
	- Youtube: https://www.youtube.com/playlist?list=PLQMs5svASiXM4AlcVLaX0LxA6APEWJPKZ


## Files:

#### Code:

* main.cpp
	- generated by Qt
	- nothing was changed    
	
* Widget Class:
	- widget.h and widget.cpp
		- basically only constructs and shows the scene
	- widget.ui
		- form for the User interface
		- I only used this to add the graphicsView and change its size    
	    
* Scene Class : scene.h and scene.cpp
	- creates/handles the sprites on the scene
	- stops and starts the game
	- also handles the user input    
	
* Player Sprite Class : playersprite.h and playersprite.cpp
	- creates the playerSprite
	- fump and fall Animations    
	
* PillarItem Class : pillaritem.h and pillaritem.cpp
	- continously creates pillar items
	- right to left animation
	- self-deleting
	- checks for collisions with playersprite  
	
#### Binaries:

* Images folder:
	- contains the .png for the sprites (player and pillars) 
	- 5 sprites for the player (only 3 are currently used for the animation)
	- 1 sprite for the pillar 

#### Other: 

* Game_Project.pro
	- program/project file generated by Qt
	
* Resource.qrc
	- lists the reasources/images I used and their paths

* Game_Project_en_US.ts 
	- generated by Qt, used for translations

* .gitignore
	- generated by Qt

* readme.md 
	- this file

* developmentdiary.md
	- I tried to log what I've done for this project, but it's very sparse

## The Development

For a brief timeline of my work: [developmentdiary.md](https://bitbucket.org/nmruelos/game-project/src/master/DevelopmentDiary.md)

For this project, I started out by following a tutorial on [youtube](https://www.youtube.com/playlist?list=PLQMs5svASiXM4AlcVLaX0LxA6APEWJPKZ) (from above) and adapting it to my own needs. As I continued with the project, I became more comfortable with branching away from the tutorial and trying to code it myself. I still relied on it on key elements, however, like collision detection and creating animations. Even with the tutorial, however, I was still pretty confused on the mechanics and aspects as he was going very quickly through them. I ended up learning most things straight from the [Qt website](https://doc.qt.io/qt-5/):    

Q_Property was the probably the most interesting. It's a macro that automatically makes getters, setters, and the like for a variable. It was needed to work with QPropertyAnimation, whiched I used to animate the sprites and pillars. I attempted to create animations myself at first by cycling through sprites, but the ease of use (and understanding) of QPropertyAnimation made me quickly change. I also learned about QTimer alongside it.    

I also got very familiar with QPixmap/QGraphicsPixmapItem. I am still not sure what the difference is mechanics-wise with QImage, but I prefered it because I did not have to explicitly call for a painter device.    

This project also required the use of many custom slots and signals. I specifically had the PillarItem class communicate through these as many objects of that class were quickly created and destroyed, so using a pointer and function (ex. `pillar1->func()`) was not available. I also practiced using lambda functions inside connections, and I realized I can use this to tie a function(s) to a signal without making it a slot. The lambda function also allowed me to create more connections inside of a connection (see `void Scene::addpillars()`).   

Back on the topic of the PillarItem class, I created objects of it using the `new` keyword but did not pair it with `delete`. Neither did I use RAII/SBRM. Instead, the pillar objects would self-delete after the end of their animations. I found this to be a really interesting way of memory management. Otherwise, I did not have time to code my own destructors, but I believe that most things eventually ended up under management of a scope object (to the scene, which was passed to the widget, etc).    

As a side note, the PillarItem class also contained the piece of code I'm most proud of figuring out (and wasn't in the tutorial!). The coordinate system of the scene is effectively "upside-down" with (0,0) at the upperleft corner of the scene, and the point where the pillars where set at was also at their upperleft corner. This made the math a bit tricky, but I managed to figure out how to properly scale the PillarItem to the ground from a random point so that the pillars would have random heights.      

Finally, I am slowly starting to get comfortable with Git. I had a bit of trouble finding good places to commit, especially when I was stuck on bugs or incomplete code. There was a week of nothing because I couldn't figure out why my sprite wasn't showing (it turned out to be that it was simply too big). `git commit --amend` quickly became my best friend in all of this. As well as resetting the branch when I got frustrated with a bit of code. At the very least, I am comfortable with using git inside of Qt.     


#### Further Development

I created the little slime-charater myself, and if I had more time, I also wanted to draw out and actual background and obstacles.     

Also, rather than pillars, I would have prefered to use different kinds of obstacles (trees, birds, bushes, etc). I think it would be interesting to turn the PillarItem Class into a base class instead, from which I can inherit from to create different kinds of objects with the collisions and animations already set up.    

I also wanted to make the screen resizable, which you can see in how I took the width and height of the scene from the UI rather than setting it myself. I couldn't quite figure out how to make it work in the time I had (perhaps emitting a signal whenever the screen was changed?).    

Also, the only button used for this was the spacebar. I could have added mouseclicks, perhaps, or more movement to the player sprite.    
