#include "scene.h"

#include "playersprite.h"
#include "pillaritem.h"

#include <QObject>
#include <QKeyEvent>
    //keyPressEvent(QKeyEvent*)
#include <QTimer>
    //addPillars()

/* *************************************************
   Scene Class Implementations
   ************************************************* */

/* ~~~~~~~~~~~~~~~~~ Constructor ~~~~~~~~~~~~~~~~~~~~ */
Scene::Scene(QObject *parent, qreal w, qreal h)
    : QGraphicsScene(parent)
    , width (w)
    , height (h)
    , ground (.8 * h) //80% from the top
    , status(true)
    , score (0)
    , score_label(new QGraphicsTextItem("Score: 0"))

{
    /* Fill QGraphicsScene,
     * but leave room for widget margins.
     * (0,0) is at the uppermost left corner */
    this->setSceneRect(0,0, width, height);

    //The Ground (QGraphicsLineItem)
    this->addLine(0,ground,width,ground,QPen(Qt::green));

    //Score Label
    score_label->setScale(2);
    this->addItem(score_label);

    //Player Sprite
        //-> move to initialization list?
    player = new playerSprite(width, height);
    this->addItem(player);

    //Start the Game
    new_game();
}

/* ~~~~~~~~~~~~~~~~~ Start New Game ~~~~~~~~~~~~~~~~~~~~ */
void Scene::new_game()
{
    emit game_started();
        //captured by PillarItems
        //deletes existing pillars

    //set sprite back to ground w/ proper image
    player->reset_sprite();

    //Reset Score
    score = 0 ;
    score_label->setPlainText("Score: 0");

    //Change game state
    status = true;

    //Restart pillars
    addpillars();
}

/* ~~~~~~~~~~~~~~~~~ Keyboard/User Inputs ~~~~~~~~~~~~~~~~~~~~ */
/* So far, the spacebar is the only key used
 * to both jump and resart the game          */
void Scene::keyPressEvent(QKeyEvent *event)
{
    if (status) { //if game has started
        switch(event->key()) {
        case Qt::Key_Space:
            player->jump();
            break;
        }
    }
    else { //status == false or game is paused
        switch(event->key()) {
        case Qt::Key_Space:
            //Restart game
            this->new_game();
            break;
        }
    }
}

/* *************************************************
 *  Scene Class: Private Slots                       */

/* ~~~~~~~~~~~~~~~~~ Stop Game ~~~~~~~~~~~~~~~~~~~~ */
void Scene::pause_game()
{
    status = false;

    player->pause_sprite();

    pillartimer->stop(); //stop creation of more pillars

    emit game_paused();
        //captured by PillarItems
        //pauses ALL existing pillars
        //rather than just the one that collided
}

/* ~~~~~~~~~~~~~~~~~ Score++ ~~~~~~~~~~~~~~~~~~~~ */
//When pillar is deleted
void Scene::increase_score()
{
    score++;
    score_label->setPlainText("Score: " + QString::number(score));
}


/* *************************************************
 *  Private Member Functions                        */

/* ~~~~~~~~~~~~~~~~~ Add new Pillars ~~~~~~~~~~~~~~~~~~~~ */
void Scene::addpillars()
{
    //Pillar Timer
    /* created  here rather than in constructor,
     * otherwise, the pillars seem to stack on top
     * of one another when game is restarted       */
    pillartimer = new QTimer(this);

    //New Pillar whenever 'pillartimer' runs out
    //uses lambda function! :)
    connect(pillartimer, &QTimer::timeout, [=](){

        //create pillarItem
        PillarItem * pillarItem = new PillarItem( width, ground );

        //add to scene
        this->addItem(pillarItem);

        //connect deletion to score
        connect(pillarItem, SIGNAL (passed_player()),
                this, SLOT (increase_score() ));

        //stop game when collision to player
        connect(pillarItem, SIGNAL( collision() ),
                this, SLOT (pause_game() ) );
        connect(this, SIGNAL( game_paused() ),
                pillarItem, SLOT (pause_pillars() ) );

        //delete existing pillars when game is restarted
        connect(this, SIGNAL( game_started() ),
                pillarItem, SLOT (delete_pillars() ) );
    });

    //restart timer
    pillartimer->start(1000);
}
