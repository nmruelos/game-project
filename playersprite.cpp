#include "playersprite.h"

/* *************************************************
   Player Sprite Class Implementations
   ************************************************* */

/* ~~~~~~~~~~~~~~~~~ Constructor ~~~~~~~~~~~~~~~~~~~~ */
playerSprite::playerSprite(qreal w, qreal h)
    : base_x (.1 * w)
    , base_y (.7 * h)
    , max_jump (.05 * h)
    , m_current_y (base_y)
{
    setPixmap(QPixmap(":/images/Images/Slime_1.png"));

    setPos( base_x, m_current_y );

    //Animation for Jumping
    jump_animation = new QPropertyAnimation(this, "current_y", this);
    jump_animation->setStartValue(base_y);
        //start at ground
    jump_animation->setEndValue(max_jump);
        //jump height
    jump_animation->setDuration(400);
        //speed
    jump_animation->setEasingCurve(QEasingCurve::OutQuad);
        //fast to slow as it approaches the top

    //Animation for Falling
    fall_animation = new QPropertyAnimation(this, "current_y", this);
    //start value determined later (when fall() is called)
    fall_animation->setEndValue(base_y);
        //end at ground
    fall_animation->setDuration(375);
        //speed, faster than jumping bc gravity
    fall_animation->setEasingCurve(QEasingCurve::InQuad);
        //slow to fast as it approaches the bottom


    connect(jump_animation, &QPropertyAnimation::finished, [=](){
        fall();
    });

    connect(fall_animation, &QPropertyAnimation::finished, [=](){
        setPixmap(QPixmap(":/images/Images/Slime_1.png"));
    });

}

/* ~~~~~~~~~~~~~~ Accessor: y-coordinate ~~~~~~~~~~~~~~~~~ */
qreal playerSprite::current_y() const
{
    return m_current_y;
}

/* ~~~~~~~~~~~~~~ Setter: y-coordinate ~~~~~~~~~~~~~~~~~ */
void playerSprite::set_current_y(qreal y)
{
    m_current_y = y;
    setPos(base_x,m_current_y);
}

/* ~~~~~~~~~~~~~~ Freeze in Place ~~~~~~~~~~~~~~~~~ */
void playerSprite::pause_sprite()
{
    jump_animation->stop();
    fall_animation->stop();
}

/* ~~~~~~~~~~~~~~ Reset Sprite to base ~~~~~~~~~~~~~~~~~ */
void playerSprite::reset_sprite()
{
    setPixmap(QPixmap(":/images/Images/Slime_1.png"));

    m_current_y = base_y;
    setPos(base_x,m_current_y);
}

/* ~~~~~~~~~~~~~~ yAnimation going up ~~~~~~~~~~~~~~~~~ */
void playerSprite::jump()
{
    //disables double-jump
    if (m_current_y != base_y) { return; }

    setPixmap(QPixmap(":/images/Images/Slime_2.png"));
    jump_animation->start();

}

/* ~~~~~~~~~~~~~~ yAnimation going down ~~~~~~~~~~~~~~~~~ */
void playerSprite::fall()
{
    setPixmap(QPixmap(":/images/Images/Slime_4.png"));

    fall_animation->setStartValue(m_current_y);
    fall_animation->start();

}
