#ifndef PLAYERSPRITE_H
#define PLAYERSPRITE_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>

class playerSprite : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

    //For Animation on y-axis:
    Q_PROPERTY(qreal current_y //for y-coordinate of sprite
               READ current_y  //accessor
               WRITE set_current_y //setter
               )
public:
    //Constructor
    explicit playerSprite( qreal w = 620, qreal h = 480);

    //Accessor
    qreal current_y() const;//From Q_Property

    void jump();//starts jump animation
    void fall();//starts fall animation

    void pause_sprite();//stops all animation
    void reset_sprite();//resets x-y coordinates + pixmap

public slots:
    void set_current_y(qreal x); //From Q_Property

private:
    /* Note: all x-y coordinates are calculated
     * at the sprite's upper left corner
     */
    qreal base_x; //unchanging x position: .1 * width
    qreal base_y; //ie, ground : .7 * height.
                  //Not equal to scene.ground, accounts for sprite size
    qreal max_jump; // .05 * height

    qreal m_current_y; //current y-coordinate

    QPropertyAnimation * jump_animation;
    //QProperty Animation * transition; //add later, maybe
    QPropertyAnimation * fall_animation;

};

#endif // PLAYERSPRITE_H
