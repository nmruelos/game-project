# Development Log

Note: This is mostly just a timeline of what I did. For a better summary of what I've learned, please check the readme.md at the bottom. 

Here's what I've done for this project so far (oldest to newest):


#### 13 May 2020: 

- Started project
- Created readme file
- Followed https://www.youtube.com/watch?v=LkISZOsogyc 
   - to create scene .cpp and .h. 
   - add graphicsView for main scene


#### 16 May 2020

- Built Pillar Item (https://www.youtube.com/watch?v=rZvV75B3EDU)
	- Learned about QgraphicsView, QgraphicsScene (https://www.youtube.com/watch?v=b35JF4LqtBs)
- Added Resources Folder to Project

####  17 May 2020
- Started on player sprite (https://www.youtube.com/watch?v=ib0JWcn1E0w)


#### 23 May 2020
- Restarted Player Sprite (Created new branch)
- Followed https://www.youtube.com/watch?v=ib0JWcn1E0w
	- stopped at 11:11
- Finally figured out that my problem was the size of the image (slime.png)

#### 30 May 2020
- fixed animations
- started on responses to key presses

#### 3 June 2020
- Added some animation 
	- originally tried using a sprite sheet, but after much confusion, I resorted to individual png files
- altered falling and jumping mechanics
	- still very convoluted, but they're starting to work the way I want to

#### 4 June 2020
- Merged Sprite Branch to master
- Cleaned up widget.cpp and transfered some stuff to scene.cpp
- Started working on a way for everything to resize nicely
	- used qreal instead of int bc I'll be dealing with ratios
- Cleaned "playerSprite"

#### 5 June 2020
- Revisited PillarItem to figure out what I want to do with it
- Learned about the Property System (https://doc.qt.io/qt-5/properties.html)
	- while its not super useful in this project, its easy enough for me to use
- Learned how to use QAnimation, QRandomGenerator

#### 6 June 2020
- Changed animations in playerSprite to QPropertyAnimation

#### 8 June 2020
- Learned how check for collisions using https://www.youtube.com/watch?v=XQs3aWmMDxg

#### 13 June 2020
- Paused Game when collision occurs

#### 14 June 2020
- Added a tracker for score
- Cleaned the files 
	- deleted qDebugs
	- added comments
	- reorganized code, etc.
- Edited readme
