#ifndef PILLARITEM_H
#define PILLARITEM_H

#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>

class PillarItem : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

    //For Animation on x-axis:
    Q_PROPERTY(qreal current_x //the property
               READ current_x  //accessor
               WRITE set_current_x //setter
               )
public:
    //Constructor
    explicit PillarItem(qreal w = 480, qreal g = 384);

    //virtual ~PillarItem();

    //accessor
    qreal current_x() const; //From Q_Property

    //compare position+area w/ player sprite
    void check_collisions();

private slots:
    void set_current_x(qreal x); //From Q_Property
    void pause_pillars();
    void delete_pillars();

signals:
    void collision();
    void passed_player();

private:
    //The Sprite
    QGraphicsPixmapItem * pillar;

    //Animation on x-axis
    QPropertyAnimation * xAnimation;

    qreal start_x; //pillar appears right-hand edge

    qreal ground; //used for calculation of height
    qreal pillar_height; //Used for y-position

    qreal m_current_x; //current x coordinate
};

#endif // PILLARITEM_H
