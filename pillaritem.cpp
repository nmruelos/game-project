#include "pillaritem.h"

#include "playersprite.h"
    //check_collionsions()

#include <QRandomGenerator>
    //pillar_height, xAnimation
#include <QGraphicsScene>
    //xAnimation - removal of pillar
#include <QList>
    //check_collisions()

/* *************************************************
   PillarItem Class Implementations
   ************************************************* */

/* ~~~~~~~~~~~~~~~~~ Constructor ~~~~~~~~~~~~~~~~~~~~ */
PillarItem::PillarItem(qreal w, qreal g)
    : pillar(new QGraphicsPixmapItem(QPixmap(":/images/Images/pillar-pillar.png")))
    , start_x(w)
    , ground(g)
    , m_current_x(start_x)
{
    //Set Height of Pillar
    pillar_height = QRandomGenerator::global()->bounded(115,ground-60);
        /* pillar_height is actually a y-coordinate,
         * which is generated randomly between/"bounded" from
         * the top (y-coordinate: 0) + 115 (for sprite-jumping room)
         * and the ground + 60 (so the pillar isn't tiny/unseeable).
         * The bigger the number, the smaller the pillar will be.   */

    //Pillar scaled then shown
    setPixmap(pillar->pixmap().scaledToHeight(ground - pillar_height));
        /* The Pillar is scaled to lenght between the pillar_height
         * and the ground.
         * It is "ground + (-pillar_height)" because (0,0)
         * is at the upperleft corner, so ground is actually greater
         * than the height.
         * The pixmap y-coordinate (also at its upperleft corner) is
         * first generated at pillar_height, and then
         * the pixmap is expanded to reach the ground.
                * This happens in 'set_current_x()'
                * during setPos(x-coor, y-coor)                       */

    //Pillar Animation: Left to Right
        //Uses Q_Property
    xAnimation = new QPropertyAnimation(this, "current_x", this);
    xAnimation->setStartValue(start_x);
        //Appears at Left-Edge
    xAnimation->setEndValue(-15);
        //Deleted Off-screen
    xAnimation->setDuration(QRandomGenerator::global()->bounded(1650,1900));
        //Minor Variations on Speed

    //Self_Deleting when finished
    connect(xAnimation, &QPropertyAnimation::finished, [=](){
        emit passed_player();
            //captured by scene, increases score
            //note...this really isn't the best place to put this...

        scene()->removeItem(this);
        delete this;
    });

    //Start animation
    xAnimation->start();
}

//PillarItem::~PillarItem() {} //no time to code... :(

/* ~~~~~~~~~~~~~~~~~ Accessor: x-coordinate ~~~~~~~~~~~~~~~~~~~~ */
qreal PillarItem::current_x() const
{
    return m_current_x;
}

/* ~~~~~~~~~~~~~~ Check collision with Player Sprite ~~~~~~~~~~~~~~~~~ */
void PillarItem::check_collisions()
{
    //list all items in collision with this
    QList<QGraphicsItem *> colliding_items = this->collidingItems();

    //Check if any of the items is the Player Sprite
    foreach ( QGraphicsItem *item, colliding_items){
        playerSprite * sprite = dynamic_cast<playerSprite*>(item);
        if ( sprite ) {
            emit collision();
                //captured by the scene
                //pauses game/ causes game_over
        }
    }
}

/* *************************************************
 *  Scene Class: Private Slots                       */

/* ~~~~~~~~~~~~~~~~~ Setter: x-coordinate ~~~~~~~~~~~~~~~~~~~~ */
void PillarItem::set_current_x(qreal x)
{
    m_current_x = x;
    setPos(m_current_x, pillar_height);
        //sets properly scaled image

    //check for collisions as pillar moves
    check_collisions();
}

/* ~~~~~~~~~~~~~~~~~ Stop Animation ~~~~~~~~~~~~~~~~~~~~ */
void PillarItem::pause_pillars()
{
    xAnimation->stop();
}

/* ~~~~~~~~~~~~~~~~~ Delete pillar ~~~~~~~~~~~~~~~~~~~~ */
void PillarItem::delete_pillars()
{
    delete this;
}
