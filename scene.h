#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>

#include "playersprite.h"

class Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    //Constructor
    explicit Scene(QObject *parent = nullptr, qreal w = 620, qreal h = 460);

    //Resets Everything and Starts the game
    void new_game();

    //Keyboard Inputs
    void keyPressEvent(QKeyEvent *event) override;

private slots:
    void pause_game(); //stops all animations
    void increase_score(); //score++

signals:
    //No need for Implementations
    void game_started(); //or game_restarted
    void game_paused(); //or game_over

private:
    //Scene Dimensions
        /* (0,0) is at the uppermost left corner
         * positive numbers going down and to the right */
    qreal width; //or x-axis length
    qreal height; //or y-axis length
    qreal ground; //where sprite lands, 80% from the top

    //Game Status
    bool status; //true for game looping, false for stopped
        //mainly used in keyPressEvent()

    //Current Score
    int score; //# of pillars passed
    QGraphicsTextItem * score_label; //displays score

    //Player Sprite (PixmapGraphicsItem)
    playerSprite * player;

    //Obstacle Sprites (PixmapGraphicsItem)
    void addpillars(); //Continuously adds pillars
    QTimer * pillartimer; //used to space pillars
};

#endif // SCENE_H
